package com.prueba.app.rest.dto;

public class AdditionalInfoDto {
	
	 private String phoneNumber;
	 private String dateOfBirth;
	 private String maritalStatus;
	 private int numberOfChildren;
	 private String observation;
	 
	public AdditionalInfoDto(String phoneNumber, String dateOfBirth, String maritalStatus, int numberOfChildren,
			String observation) {
		super();
		this.phoneNumber = phoneNumber;
		this.dateOfBirth = dateOfBirth;
		this.maritalStatus = maritalStatus;
		this.numberOfChildren = numberOfChildren;
		this.observation = observation;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public int getNumberOfChildren() {
		return numberOfChildren;
	}

	public void setNumberOfChildren(int numberOfChildren) {
		this.numberOfChildren = numberOfChildren;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}
	
	
	 
	 

}
