package com.prueba.app.rest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prueba.app.rest.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	List<User> findByMaritalStatusAndAgeGreaterThan(String maritalStatus, Integer age);
    List<User> findByMaritalStatus(String maritalStatus);
    List<User> findByAgeGreaterThan(Integer age);
	

}
