package com.prueba.app.rest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.prueba.app.rest.dto.AdditionalInfoDto;
import com.prueba.app.rest.entities.User;
import com.prueba.app.rest.services.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public void saveUsers(@RequestBody List<User> users) {
        userService.saveUsers(users);
    }

    @DeleteMapping("/{id}")
    public void deleteLogicalUser(@PathVariable Long id) {
        userService.deleteLogicalUser(id);
    }

    @PostMapping("/{id}/additional-info")
    public void addAdditionalInfoToUser(@PathVariable Long id, @RequestBody AdditionalInfoDto additionalInfo) {
        userService.addAdditionalInfoToUser(id, additionalInfo);
    }

    @GetMapping
    public List<User> getUsersByFilter(@RequestParam(required = false) String maritalStatus, @RequestParam(required = false) Integer age) {
        return userService.getUsersByFilter(maritalStatus, age);
    }
}