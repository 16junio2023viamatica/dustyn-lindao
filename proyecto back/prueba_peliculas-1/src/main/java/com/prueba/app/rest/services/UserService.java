package com.prueba.app.rest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.app.rest.dto.AdditionalInfoDto;
import com.prueba.app.rest.entities.User;
import com.prueba.app.rest.repository.UserRepository;

@Service
public class UserService {
	
	private UserRepository userRepository;
	
	@Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
	
	 public void saveUsers(List<User> users) {
	        userRepository.saveAll(users);
	    }
	 
	 public void deleteLogicalUser(Long id) {
		    // Lógica para realizar la eliminación lógica del usuario por su ID
		    User user = userRepository.findById(id).orElse(null);
		    if (user != null) {
		        user.setStatus("Inactivo");
		        userRepository.save(user);
		    }
		}

	 
	   public void addAdditionalInfoToUser(Long id, AdditionalInfoDto additionalInfo) {
	        // Lógica para agregar información adicional al usuario por su ID
	        User user = userRepository.findById(id).orElse(null);
	        if (user != null) {
	            user.setPhoneNumber(additionalInfo.getPhoneNumber());
	            user.setDateOfBirth(additionalInfo.getDateOfBirth());
	            user.setMaritalStatus(additionalInfo.getMaritalStatus());
	            if (additionalInfo.getMaritalStatus().equals("Casado")) {
	                user.setNumberOfChildren(additionalInfo.getNumberOfChildren());
	            } else {
	                user.setNumberOfChildren(0);
	            }
	            user.setObservation(additionalInfo.getObservation());
	            userRepository.save(user);
	        }
	    }
	   
	   public List<User> getUsersByFilter(String maritalStatus, Integer age) {
	        // Lógica para obtener usuarios por filtro (estado civil y edad)
	        if (maritalStatus == null && age == null) {
	            return userRepository.findAll();
	        } else if (maritalStatus != null && age != null) {
	            return userRepository.findByMaritalStatusAndAgeGreaterThan(maritalStatus, age);
	        } else if (maritalStatus != null) {
	            return userRepository.findByMaritalStatus(maritalStatus);
	        } else {
	            return userRepository.findByAgeGreaterThan(age);
	        }
	    }
	

}
