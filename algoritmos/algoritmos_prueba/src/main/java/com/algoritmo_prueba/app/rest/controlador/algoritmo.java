package com.algoritmo_prueba.app.rest.controlador;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.algoritmo_prueba.app.rest.modelo.Placas;

@RestController
public class algoritmo {
	
	@GetMapping(value = "/generarPlacas")
	public List<String> generatePlates(@RequestParam String province, @RequestParam String type, @RequestParam int blocks) {
	    List<String> placas = new ArrayList<>();

	    for (int i = 0; i < blocks; i++) {
	        Placas placa = new Placas(type, type, i);
	        placa.setProvincias(province);
	        placa.setTipo(type);
	        placa.setNumero(generateRandomNumber(3, 9));

	        String generarPlacas = generarPlacasNumber(placa);
	        placas.add(generarPlacas);
	    }

	    return placas;
	}


	private int generateRandomNumber(int min, int max) {
	    return new Random().nextInt((max - min) + 1) + min;
	}
	private String generarPlacasNumber(Placas placas) {
	    StringBuilder placaNumero = new StringBuilder();
	    placaNumero.append(placas.getProvincias().substring(0, 1));
	    placaNumero.append(placas.getTipo().equals("Vehículos comerciales") ? "A" : getRandomCharExcept("APM"));
	    placaNumero.append(placas.getNumero());
	    placaNumero.append("-");
	    placaNumero.append(generateRandomNumber(0, 9));
	    placaNumero.append(generateRandomNumber(0, 9));
	    placaNumero.append(generateRandomNumber(0, 9));
	    placaNumero.append(generateRandomNumber(0, 9));

	    return placaNumero.toString();
	}


	private char getRandomCharExcept(String characters) {
	    String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    List<Character> validChars = alphabet.chars()
	            .mapToObj(c -> (char) c)
	            .filter(c -> !characters.contains(String.valueOf(c)))
	            .collect(Collectors.toList());

	    return validChars.get(new Random().nextInt(validChars.size()));
	}
	
	 @GetMapping("/manipulateString")
	    public String manipulateString(@RequestParam String cadena) {
	        int longitud = cadena.length();
	        
	        if (longitud % 2 == 0) {
	            // Longitud par: intercambiar las partes de la cadena
	            int mitad = longitud / 2;
	            String parte1 = cadena.substring(0, mitad);
	            String parte2 = cadena.substring(mitad);
	            return parte2 + parte1;
	        } else {
	            // Longitud impar: indicar la longitud
	        	return "La cadena ingresada (" + cadena + ") tiene una longitud impar: " + longitud;
	        }
	    }

	

}
