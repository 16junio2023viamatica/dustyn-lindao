package com.algoritmo_prueba.app.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlgoritmosPruebaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlgoritmosPruebaApplication.class, args);
	}

}
