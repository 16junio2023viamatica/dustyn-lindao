package com.algoritmo_prueba.app.rest.modelo;

public class Placas {
	
private String provincias;
private String tipo;
private int numero;


public Placas(String provincias, String tipo, int numero) {
	super();
	this.provincias = provincias;
	this.tipo = tipo;
	this.numero = numero;
}


public String getProvincias() {
	return provincias;
}


public void setProvincias(String provincias) {
	this.provincias = provincias;
}


public String getTipo() {
	return tipo;
}


public void setTipo(String tipo) {
	this.tipo = tipo;
}


public int getNumero() {
	return numero;
}


public void setNumero(int numero) {
	this.numero = numero;
}








}
